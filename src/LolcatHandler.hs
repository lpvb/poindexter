{-# LANGUAGE OverloadedStrings #-}
module LolcatHandler (handler) where

--------------------------------------------------------------------------------
import Control.Monad.Trans (liftIO)
import Data.ByteString (ByteString)
import Data.Text (append)
import qualified Data.Text.Encoding as T
import Text.XmlHtml
import Text.XmlHtml.Cursor
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
--------------------------------------------------------------------------------
import NumberSix.Bang
import NumberSix.Irc
import NumberSix.Message
import NumberSix.Util.Error
import NumberSix.Util.Http

--------------------------------------------------------------------------------
lolcat :: Text -> IO Text
lolcat query = do
  result <- httpScrape Html url id $ \cursor -> do
    translation <- findRec (byTagNameAttrs "textarea" [("id","to")]) cursor
    return $ T.encodeUtf8 $ nodeText $ current translation
  maybe randomError return (fmap decodeUtf8 result) 
  where url = "http://speaklolcat.com/?from=" <> urlEncode query

handler :: UninitializedHandler
handler = makeBangHandler "Lolcat" ["!lol"] $ liftIO . lolcat
