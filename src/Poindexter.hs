-- | Number six sample configuration file
--
{-# LANGUAGE OverloadedStrings #-}
module Main where

import System.Environment
import System.Process

import Control.Monad.Trans (liftIO)
import Control.Monad
import Control.Applicative

import Data.List.Split (splitOn)
import Data.List ((\\))
import Data.Word
import qualified Data.Text as T
import qualified Data.Map as M
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString as B

import Network.BSD
import qualified Network.Socket as S

import NumberSix
import NumberSix.Handlers
import NumberSix.Irc
import NumberSix.Bang
import NumberSix.Util
import NumberSix.Message
import NumberSix.Handlers.Op
import NumberSix.Handlers.Resto
import NumberSix.Handlers.Topic
import NumberSix.Handlers.Voice
import NumberSix.Handlers.TryHaskell

import LolcatHandler

interjectHandler :: UninitializedHandler
interjectHandler = makeHandler "interject" $ return $ onBangCommand "!interject" $ onGod $ do
  write "I'd just like to interject for a moment. What you're referring to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX."
  write "Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called Linux, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project."
  write "There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system." 
  write "Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called Linux distributions are really distributions of GNU/Linux."

leaveHandler :: UninitializedHandler
leaveHandler = makeHandler "Leave" . return . onBangCommand "!leave" . onGod $
  do chan <- getChannel
     writeMessage "PART" [chan]

--------------------------------------------------------------------------------
jiveHandler :: UninitializedHandler
jiveHandler = makeHandler "Jive" $ return $ onBangCommand "!jive" $
  write =<<  fmap T.pack . liftIO . readProcess "jive" [] =<< 
    (fmap T.unpack getBangCommandText)

chefHandler :: UninitializedHandler
chefHandler = makeHandler "Chef" $ return $ onBangCommand "!chef" $
  write =<<  fmap T.pack . liftIO . readProcess "chef" [] =<< 
    (fmap T.unpack getBangCommandText)

valspeakHandler :: UninitializedHandler
valspeakHandler = makeHandler "Valspeak" $ return $ onBangCommand "!val" $
  write =<<  fmap T.pack . liftIO . readProcess "valspeak" [] =<< 
    (fmap T.unpack getBangCommandText)

piglatinHandler :: UninitializedHandler
piglatinHandler = makeHandler "PigLatin" $ return $ onBangCommand "!piglatin" $
  write =<<  fmap T.pack . liftIO . readProcess "piglatin" [] =<< 
    (fmap T.unpack getBangCommandText)

jivePrivHandler :: UninitializedHandler
jivePrivHandler = makeHandler "JivePriv" $ return $ onBangCommand "!j" $
  uncurry writeChannel . breakWord =<<
    fmap T.pack . liftIO . readProcess "jive" [] =<< 
      (fmap T.unpack getBangCommandText)

mueval :: T.Text -> IO T.Text
mueval expr = fmap T.pack $ 
  readProcess "mueval" ["--expression", T.unpack expr] []

muevalHandler :: UninitializedHandler
muevalHandler = makeBangHandler "Mueval" [">", "!haskell"] $ liftIO . mueval

myHandles :: [UninitializedHandler]
myHandles =
  [ interjectHandler
  , jiveHandler
  , jivePrivHandler
  , leaveHandler
  , chefHandler
  , piglatinHandler
  , valspeakHandler
  , muevalHandler
  , LolcatHandler.handler
  ]

unwantedHandles :: [UninitializedHandler]
unwantedHandles =
  [ NumberSix.Handlers.Op.handler
  , NumberSix.Handlers.Resto.handler
  , NumberSix.Handlers.Topic.handler
  , NumberSix.Handlers.Voice.handler
  , NumberSix.Handlers.TryHaskell.handler
  ]

parseArgs :: [String] -> M.Map T.Text T.Text
parseArgs args = M.fromListWith combinator pairList
  where combinator :: T.Text -> T.Text -> T.Text
        combinator s1 s2 = s1 <> "," <> s2
        args' = map T.pack args
        pairList = zip args' (tail args')
  
main :: IO ()
main = do
  args <- getArgs
  let argmap = parseArgs args
      find = M.findWithDefault
  numberSixWith ((handlers ++ myHandles) \\ unwantedHandles) IrcConfig
    { ircNick        = find "poindexter" "--nick" argmap
    , ircRealName    = find "Utility Bot" "--name" argmap
    , ircChannels    = map T.pack . splitOn "," . T.unpack $ find "#poindexter" "--chan" argmap
    , ircHost        = find "irc.freenode.net" "--serv" argmap
    , ircPort        = (read . T.unpack $ find "6667" "port" argmap) :: Int
    , ircGodPassword = ""
    , ircDatabase    = "poindexterbot.db"
    , ircNickServ    = 
        let pass = find "" "--pass" argmap
        in  Just ("nickserv", "IDENTIFY " <> pass)
--    , ircProxy       = fmap ((\(u,p) -> (u,read $ tail p :: Int)) . break (==':') . BC.unpack) $ M.lookup "--proxy" argmap
    }
