# Poindexter
##Description
This is a specialized IRC bot running off of the [number-six library](https://github.com/jaspervdj/number-six).

##Usage
Several command options are available:
   
    --serv
    --chan
    --proxy   (e.g.) 'localhost:9332'
    --nick
    --pass
    --chan
    
